
import React from 'react';
import { BrowserRouter as Router, Route, Routes , Link} from 'react-router-dom';
import SearchBar from './components/SearchBar';
import Home from './components/Home';
import ManageData from './components/ManageData';
import { Navbar } from 'flowbite-react';

const NavbarComponent = () => {
  return (
    <Router>
      <div>
        <nav className="bg-blue-500 p-4">
          <div className="container mx-auto">
            <div className="flexflex justify-between items-center">
             <h1 className="text-white text-2xl font-semibold">Quiz 3 | Salsabillah Putri Dielika</h1>
              <div className="space-x-4">
                <Link to="/" className="text-white">Home</Link>
                <Link to="/manage" className="text-white">Manage Data</Link>
                 </div>
              </div>
          </div>
        </nav>
      </div>
      <div className="space-x-4">
        <SearchBar/>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/manage" element={<ManageData />} />
        </Routes>
      </div>
    </Router>
  );
   
};


const SearchBarComponent = () => {
  return (
    <>
    <div className="bg-gray-100 p-4">
      <div className="container mx-auto">
        <input
          type="text"
          placeholder="Find your data that you need!"
          className="w-full p-2 rounded-md border border-gray-300"
        />
      </div>
    </div>
    </>
  );
};

// ... rest of the code


const GameCard = ({ title, year, description, consoleInfo, price }) => {
  return (
    <div className="bg-white p-4 shadow-md rounded-md">
      <h2 className="text-xl font-semibold">{title}</h2>
      <p className="text-gray-500">{year}</p>
      <p className="text-gray-700">{description}</p>
      <p className="text-gray-500">{consoleInfo}</p>
      <p className="text-green-600 font-semibold">{price}</p>
      <button className="bg-green-500 text-white rounded-md px-4 py-2 mt-4">Pesan Tiket</button>
    </div>
  );
};

const App = () => {
  return (
    
    <div>
      <Navbar/>
      <SearchBar />
      <div className="container mx-auto grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4 p-4">
        <GameCard
          title="Barbie as the Princess and the Pauper"
          imgSrc="https://o-cdn-cas.sirclocdn.com/parenting/images/Film_Barbie_as_the_Princess_and_t.width-800.format-webp.webp"
          year="2019"
          description="“Film Barbie yang pertama bercerita tentang seorang putri kerajaan bernama Putri Anneliese yang merupakan seorang putri raja..."
          consoleInfo="Coaction: 3.6/12.6 GB"
          price="Rp. 35.000,-"
        />
        <GameCard
          title="Barbie: Thumbelina"
          year="2017"
          description="“Bercerita tentang perjuangan peri kecil bernama Thumbelina yang tinggal di Twillerbees bersama dengan seorang gadis bernama Makena dan dua temannya...."
          consoleInfo="Size: 2.41 GB/10GB"
          price="Rp. 30.000,-"
        />
        <GameCard
          title=" Barbie: The Princess and The Popstar"
          year="2017"
          description="“Bercerita tentang kisah Tori si putri kerajaan dan Keira yang merupakan penyanyi pop, mereka berdua rupanya memiliki wajah yang mirip..."
          consoleInfo="Adventure: CAE GJ/Android & iOS"
          price="Rp. 35.000,-"
        />
        <GameCard
          title=" Barbie: A Fashion Fairytale"
          year="2010"
          description="“Salah satunya, ia baru saja dipecat dari film yang sedang dia bintangi. Ditambah lagi, dia baru saja mengalami putus cinta dengan pacarnya yang bernama Ken...."
          consoleInfo="PlayStation Portable (PSP)"
          price="Rp. 35.000,-"
        />
      </div>
    </div>
    
  );
};

export default NavbarComponent;;