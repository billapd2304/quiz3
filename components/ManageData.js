// ManageData.jsx
'use client';

import React from 'react';
import { Alert } from 'flowbite-react';

const ManageData = () => {
  return (
    <div>
      <h2 className="Bold">Manage data</h2>
      {/* Your manage data page content */}
      
      {/* Add the Alert component */}
      <Alert color="info">
        <span className="font-medium">alert!</span>No purchase history
      </Alert>
    </div>
  );
};

export default ManageData;
