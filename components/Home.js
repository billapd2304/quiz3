import React from 'react';

const GameCard = ({ title, year, description, consoleInfo, price }) => {
  return (
    <div className="bg-white p-4 shadow-md rounded-md">
      <h2 className="text-xl font-semibold">{title}</h2>
      <p className="text-gray-500">{year}</p>
      <p className="text-gray-700">{description}</p>
      <p className="text-gray-500">{consoleInfo}</p>
      <p className="text-green-600 font-semibold">{price}</p>
      <button className="bg-green-500 text-white rounded-md px-4 py-2 mt-4">Pesan Tiket</button>
    </div>
  );
};

const Home = () => {
  return (
    <div>
      <h1 className="text-white text-2xl font-semibold">Home Page</h1>
      {/* Your home page content */}
      
      {/* Include the GameCard component here */}
      <GameCard
        title="Barbie as the Princess and the Pauper"
        imgSrc="https://o-cdn-cas.sirclocdn.com/parenting/images/Film_Barbie_as_the_Princess_and_t.width-800.format-webp.webp"
        year="2019"
        description="“Film Barbie yang pertama bercerita tentang seorang putri kerajaan bernama Putri Anneliese yang merupakan seorang putri raja..."
        consoleInfo="Coaction: 3.6/12.6 GB"
        price="Rp. 35.000,-"
      />
     <GameCard
          title="Barbie: Thumbelina"
          year="2017"
          description="“Bercerita tentang perjuangan peri kecil bernama Thumbelina yang tinggal di Twillerbees bersama dengan seorang gadis bernama Makena dan dua temannya...."
          consoleInfo="Size: 2.41 GB/10GB"
          price="Rp. 30.000,-"
        />
        <GameCard
          title=" Barbie: The Princess and The Popstar"
          year="2017"
          description="“Bercerita tentang kisah Tori si putri kerajaan dan Keira yang merupakan penyanyi pop, mereka berdua rupanya memiliki wajah yang mirip..."
          consoleInfo="Adventure: CAE GJ/Android & iOS"
          price="Rp. 35.000,-"
        />
        <GameCard
          title=" Barbie: A Fashion Fairytale"
          year="2010"
          description="“Salah satunya, ia baru saja dipecat dari film yang sedang dia bintangi. Ditambah lagi, dia baru saja mengalami putus cinta dengan pacarnya yang bernama Ken...."
          consoleInfo="PlayStation Portable (PSP)"
          price="Rp. 35.000,-"
        />
    </div>
  );
};

export default Home;
